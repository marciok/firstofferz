MomondoFirstOffer = {
  init: function(){
    this.flightListReady();
    this.addCss('#customFO {text-align: center; padding: 10px}');
  },

  // Adding the custom Css
  addCss: function(cssCode) {
    var styleElement = document.createElement("style");
    styleElement.type = "text/css";
    if (styleElement.styleSheet) {
      styleElement.styleSheet.cssText = cssCode;
    } else {
      styleElement.appendChild(document.createTextNode(cssCode));
    }
    document.getElementsByTagName("head")[0].appendChild(styleElement);
  },

  // Waiting the flight list to be ready
  flightListReady: function(){
    var flightListElScan = setInterval(function(){
      var firstFlightEl = document.getElementsByClassName('segmentheader')[0];
      if (document.getElementsByClassName('active')[1] !== undefined) {
        window.clearInterval(flightListElScan);
        MomondoFirstOffer.injectButton();
      }
    },500);
  },

  // Injecting the button
  injectButton: function(){
    var flightList = document.getElementById('flight_list');
    var btnHolder = document.createElement('div');
    var btnBetterDeal = document.createElement('button');

    // Setting up 
    btnBetterDeal.className = btnBetterDeal.className + 'medium pink';
    btnBetterDeal.textContent = 'Еще дешевле';
    btnHolder.className = btnHolder.className + 'flight';
    btnHolder.id = 'customFO';
    // Appending the values
    btnHolder.appendChild(btnBetterDeal);
    // Inserting 
    flightList.getElementsByClassName('show-on-results')[0].insertBefore(btnHolder,document.getElementById('flight_results'));
    // Clicking action
    btnHolder.onclick = function(){
      window.open(MomondoFirstOffer.getURL(),'_blank');
      return false;
    };
  },

  // Extracting parameters
  getFlightData: function(){
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if (typeof query_string[pair[0]] === "undefined") {
        query_string[pair[0]] = pair[1];
      } else if (typeof query_string[pair[0]] === "string") {
          var arr = [ query_string[pair[0]], pair[1] ];
        query_string[pair[0]] = arr;
      } else {
        query_string[pair[0]].push(pair[1]);
      }
    } 
      return query_string;
  },

  // Returning the url customized
  getURL: function(){
    var flightInfo = MomondoFirstOffer.getFlightData();
    var baseURL = 'http://search.aviasales.ru/searches/new/';
    baseURL = baseURL + '?utf8=?&with_request=true&search[origin_iata]=' + flightInfo.SO0;
    baseURL = baseURL + '&search[destination_iata]=' + flightInfo.SD0;
    baseURL = baseURL + '&search[depart_date]=' + flightInfo.SDP0;
    if (flightInfo.TripType === 'oneway') {
      baseURL = baseURL + '&search[one_way]=true';
    } else {
      baseURL = baseURL + '&search[one_way]=false';
      baseURL = baseURL + '&search[return_date]=' + flightInfo.SDP1;
    }
    baseURL = baseURL + '&search[adults]=' + flightInfo.AD;
    if (flightInfo.CA !== undefined) {
      baseURL = baseURL + '&search[children]=' + flightInfo.CA.split(',').length;
    }
    baseURL = baseURL + '&search[infants]=0';
    return baseURL + '&search[trip_class]=' + flightInfo.TK + '&commit=Find';
  }

};

console.info('reading MOMONDO');
window.load = MomondoFirstOffer.init();

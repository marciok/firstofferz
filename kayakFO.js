KayakFirstOffer = {
  init: function(){
    this.flightListReady();
    this.addCss('#customFO {cursor: pointer;}');
  },

  // Adding the custom Css
  addCss: function(cssCode) {
    var styleElement = document.createElement("style");
    styleElement.type = "text/css";
    if (styleElement.styleSheet) {
      styleElement.styleSheet.cssText = cssCode;
    } else {
      styleElement.appendChild(document.createTextNode(cssCode));
    }
    document.getElementsByTagName("head")[0].appendChild(styleElement);
  },

  // Waiting the flight list to be ready
  flightListReady: function(){
    var flightListElScan = setInterval(function(){
      var flightsLoader = document.getElementsByClassName('resbodySpinnerContainer')[0];
      if (document.getElementsByClassName('flightresult')[1] !== undefined) {
        window.clearInterval(flightListElScan);
        KayakFirstOffer.injectButton();
      }
    },500);
  },


  // Injecting the button
  injectButton: function(){
    var btnHolder = document.createElement('div');
    var btnInnerHolder = document.createElement('div');
    var btnBetterDeal = document.createElement('a');
    var btnContent = document.createElement('span');

    // Setting up
    btnBetterDeal.className = btnBetterDeal.className + 'ui-button';
    btnContent.textContent = 'Еще дешевле';
    btnHolder.id = 'customFO';
    btnHolder.className = btnHolder.className + 'flightresult resultrow';
    btnInnerHolder.className = btnInnerHolder.className + 'inner';
    // Appending values
    btnHolder.appendChild(btnInnerHolder);
    btnInnerHolder.appendChild(btnBetterDeal);
    btnBetterDeal.appendChild(btnContent);
    // Inserting
    document.getElementById('listbody').insertBefore(btnHolder,document.getElementById('content_div'));
    // Click Action
    btnHolder.onclick = function(){
      window.open(KayakFirstOffer.getURL(),'_blank');
      return false;
    };
  },

  // Returning the url customized
  getURL: function(){
    var baseURL = 'http://search.aviasales.ru/searches/new/';
    baseURL = baseURL + '?utf8=?&with_request=true&search[origin_iata]=' + window.location.pathname.split('/')[2].split(',')[0];
    baseURL = baseURL + '&search[destination_iata]=' + window.location.pathname.split('/')[2].split(',')[1].replace('nearby-','');
    baseURL = baseURL + '&search[depart_date]=' + window.location.pathname.split('/')[3];
    if (document.getElementById('travel_dates-end-display').textContent === '') {
      baseURL = baseURL + '&search[one_way]=true';
    } else {
      baseURL = baseURL + '&search[one_way]=false';
      baseURL = baseURL + '&search[return_date]=' + window.location.pathname.split('/')[4];
    }
    if (window.location.pathname.split('/').length === 5) {
      baseURL = baseURL + '&search[adults]=1';
      baseURL = baseURL + '&search[children]=0';
      baseURL = baseURL + '&search[infants]=0';
      baseURL + '&search[trip_class]=economy&commit=Find';
    }
    //TODO: Get the other parameters
    return baseURL + '&commit=Find';
  }

};

// Initializing ater the window is loaded
window.load = KayakFirstOffer.init();
